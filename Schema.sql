-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-12-18 23:53:56.062

-- tables
-- Table: Cart
CREATE TABLE Cart (
    cart_id number(5)  NOT NULL,
    item_count number(2)  NULL,
    CONSTRAINT Cart_pk PRIMARY KEY (cart_id)
) ;

-- Table: Cart_products
CREATE TABLE Cart_products (
    cart_id number(5)  NOT NULL,
    product_id number(5)  NOT NULL,
    product_quantity number(3)  NOT NULL,
    CONSTRAINT Cart_products_pk PRIMARY KEY (product_id,cart_id)
) ;

-- Table: Connection_log
CREATE TABLE Connection_log (
    customer_id number(6)  NOT NULL,
    login_time date  NOT NULL,
    CONSTRAINT Connection_log_pk PRIMARY KEY (customer_id)
) ;

-- Table: Customers_CZ
CREATE TABLE Customers_CZ (
    customer_id number(6)  NOT NULL,
    address varchar2(30)  NOT NULL,
    phone char(10)  NOT NULL,
    ref_code number(6)  NULL,
    Fname varchar2(30)  NOT NULL,
    Lname varchar2(30)  NOT NULL,
    CONSTRAINT Customers_CZ_pk PRIMARY KEY (customer_id)
) ;

-- Table: Ingredients
CREATE TABLE Ingredients (
    ingredient_id number(5)  NOT NULL,
    name varchar2(20)  NOT NULL,
    restock_price number(6,2)  NOT NULL,
    stock number(5)  NOT NULL,
    CONSTRAINT Ingredients_pk PRIMARY KEY (ingredient_id)
) ;

-- Table: Login_CZ
CREATE TABLE Login_CZ (
    customer_id number(6)  NOT NULL,
    username varchar2(20)  NOT NULL,
    password varchar2(60)  NOT NULL,
    salt varchar2(60)  NOT NULL,
    CONSTRAINT Login_CZ_pk PRIMARY KEY (customer_id)
) ;

-- Table: Orders_CZ
CREATE TABLE Orders_CZ (
    cart_id number(5)  NOT NULL,
    address varchar2(30)  NOT NULL,
    total_cost number(5,2)  NULL,
    customer_id number(6)  NOT NULL,
    date_placed date  NULL,
    date_delivered date  NULL,
    CONSTRAINT Orders_CZ_pk PRIMARY KEY (cart_id)
) ;

-- Table: Products
CREATE TABLE Products (
    product_id number(5)  NOT NULL,
    name varchar2(20)  NOT NULL,
    retail number(5,2)  NOT NULL,
    image blob  NULL,
    CONSTRAINT Products_pk PRIMARY KEY (product_id)
) ;

-- Table: recipe_ingredients
CREATE TABLE recipe_ingredients (
    ingredient_id number(5)  NOT NULL,
    product_id number(5)  NOT NULL,
    CONSTRAINT recipe_ingredients_pk PRIMARY KEY (product_id,ingredient_id)
) ;

-- foreign keys
-- Reference: Cart_products_Cart (table: Cart_products)
ALTER TABLE Cart_products ADD CONSTRAINT Cart_products_Cart
    FOREIGN KEY (cart_id)
    REFERENCES Cart (cart_id);

-- Reference: Cart_products_Products (table: Cart_products)
ALTER TABLE Cart_products ADD CONSTRAINT Cart_products_Products
    FOREIGN KEY (product_id)
    REFERENCES Products (product_id);

-- Reference: Customers_Customers (table: Customers_CZ)
ALTER TABLE Customers_CZ ADD CONSTRAINT Customers_Customers
    FOREIGN KEY (ref_code)
    REFERENCES Customers_CZ (customer_id);

-- Reference: Customers_Login (table: Login_CZ)
ALTER TABLE Login_CZ ADD CONSTRAINT Customers_Login
    FOREIGN KEY (customer_id)
    REFERENCES Customers_CZ (customer_id);

-- Reference: Customers_Orders (table: Orders_CZ)
ALTER TABLE Orders_CZ ADD CONSTRAINT Customers_Orders
    FOREIGN KEY (customer_id)
    REFERENCES Customers_CZ (customer_id);

-- Reference: Ingredients_recipe_ingredients (table: recipe_ingredients)
ALTER TABLE recipe_ingredients ADD CONSTRAINT Ingredients_recipe_ingredients
    FOREIGN KEY (ingredient_id)
    REFERENCES Ingredients (ingredient_id);

-- Reference: Login_CZ_Connection_log (table: Connection_log)
ALTER TABLE Connection_log ADD CONSTRAINT Login_CZ_Connection_log
    FOREIGN KEY (customer_id)
    REFERENCES Login_CZ (customer_id);

-- Reference: Orders_Cart (table: Orders_CZ)
ALTER TABLE Orders_CZ ADD CONSTRAINT Orders_Cart
    FOREIGN KEY (cart_id)
    REFERENCES Cart (cart_id);

-- Reference: recipe_ingredients_Products (table: recipe_ingredients)
ALTER TABLE recipe_ingredients ADD CONSTRAINT recipe_ingredients_Products
    FOREIGN KEY (product_id)
    REFERENCES Products (product_id);

-- End of file.


-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2020-12-18 23:53:56.062

-- foreign keys
ALTER TABLE Cart_products
    DROP CONSTRAINT Cart_products_Cart;

ALTER TABLE Cart_products
    DROP CONSTRAINT Cart_products_Products;

ALTER TABLE Customers_CZ
    DROP CONSTRAINT Customers_Customers;

ALTER TABLE Login_CZ
    DROP CONSTRAINT Customers_Login;

ALTER TABLE Orders_CZ
    DROP CONSTRAINT Customers_Orders;

ALTER TABLE recipe_ingredients
    DROP CONSTRAINT Ingredients_recipe_ingredients;

ALTER TABLE Connection_log
    DROP CONSTRAINT Login_CZ_Connection_log;

ALTER TABLE Orders_CZ
    DROP CONSTRAINT Orders_Cart;

ALTER TABLE recipe_ingredients
    DROP CONSTRAINT recipe_ingredients_Products;

-- tables
DROP TABLE Cart;

DROP TABLE Cart_products;

DROP TABLE Connection_log;

DROP TABLE Customers_CZ;

DROP TABLE Ingredients;

DROP TABLE Login_CZ;

DROP TABLE Orders_CZ;

DROP TABLE Products;

DROP TABLE recipe_ingredients;

-- End of file.


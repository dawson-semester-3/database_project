-- Register Customer
create or replace function getHighestCustomerID
return number
as
    highestCustomer_id customers_cz.customer_id%type;
begin
    select max(customer_id) into highestCustomer_id from customers_cz;
    
    return nvl(highestCustomer_id, 1000);
end;

create or replace procedure registerCustomer(username varchar2, password varchar2, salt varchar2, myaddress varchar2, phone char, ref_code number, Fname varchar2, Lname varchar2) 
as
    newCustomer_id customers_cz.customer_id%type := getHighestCustomerID() + 1;
begin
--    select max(customer_id) into newCustomer_id from customers_cz;

    newCustomer_id := newCustomer_id;

    insert into customers_cz
        values(newCustomer_id, myaddress, phone, ref_code, Fname, Lname);
    
    insert into login_cz
        values(newCustomer_id, username, password, salt);
end;



-- adds most recent connection to connection log
create or replace procedure logConnection(username_input login_cz.username%type)
as
    newCustomer_id customers_cz.customer_id%type;
    userExists customers_cz.customer_id%type;
begin
    select customer_id into userExists 
        from connection_log
        join login_cz
            using (customer_id)
        where username = username_input;

    if userExists is not null then
        update connection_log
            set login_time = CURRENT_DATE
            where customer_id = userExists;
    else
        select customer_id into newCustomer_id from login_cz where username = username_input;
        
        insert into connection_log
            values (newCustomer_id, CURRENT_DATE);
    end if;
end;



-- Add to Cart & order
create or replace function getHighestCartID
return number
as
    highestCart_id cart.cart_id%type;
begin
    select max(cart_id) into highestCart_id from cart;
    
    return nvl(highestCart_id, 1000);
end;

create or replace procedure addToCart(cappuccinoQty number, latteQty number, espressoQty number, mintTeaQty number, blueberryMuffinQty number, strawberryCroissantQty number)
as
    newCart_id cart.cart_id%type := getHighestCartID() + 1;
    item_count_total cart.item_count%type := cappuccinoQty + latteQty + espressoQty + mintTeaQty + blueberryMuffinQty + strawberryCroissantQty;
begin
    -- create cart record before everything
    if item_count_total > 0 then
        insert into cart
            values (newCart_id, item_count_total);
    end if;
    
    
    -- create cart_products records
    if cappuccinoQty > 0 then
        insert into cart_products
            values (newCart_id, 1, cappuccinoQty);
    end if;
    
    if latteQty > 0 then
        insert into cart_products
            values (newCart_id, 2, latteQty);
    end if;
    
    if espressoQty > 0 then
        insert into cart_products
            values (newCart_id, 3, espressoQty);
    end if;
    
    if mintTeaQty > 0 then
        insert into cart_products
            values (newCart_id, 4, mintTeaQty);
    end if;
    
    if blueberryMuffinQty > 0 then
        insert into cart_products
            values (newCart_id, 5, blueberryMuffinQty);
    end if;
    
    if strawberryCroissantQty > 0 then
        insert into cart_products
            values (newCart_id, 6, strawberryCroissantQty);
    end if;
end;


create or replace function getCartValues(cart_id_input orders_cz.cart_id%type, product_id_input cart_products.product_id%type)
return number
as
    quantity cart_products.product_quantity%type;
begin
    select product_quantity into quantity
        from cart_products
        where product_id = product_id_input and cart_id = cart_id_input;
    return nvl(quantity, 0);
exception
    when no_data_found then
        return 0;
end;


-- this took 6 hours give us 100% pls
--counts customers referred by user
create or replace function hasReferredCount(username_input login_cz.username%type)
return number
as
    mycustomer_id orders_cz.customer_id%type;
    countReferred number(1);
begin
    --get customer id from username
    select customer_id into mycustomer_id from login_cz where username = username_input;
    
    --count referred
    select count(referred.ref_code) into countReferred
        from customers_cz referred
        join customers_cz referee
            on referred.ref_code = referee.customer_id
        where referee.customer_id = mycustomer_id and ROWNUM <= 3;
    
    return countReferred;
end;

-- removes ref codes from customers table
create or replace procedure removeRefCode(username_input login_cz.username%type)
as
    mycustomer_id orders_cz.customer_id%type;

    type myarray is varray(99)
        of number(4);
    customer_ids_to_delete myarray;
    ref_codes_to_delete myarray;
begin
    --get customer id from username
    select customer_id into mycustomer_id from login_cz where username = username_input;
    
    -- referee = person who gave their ref_code        
    select referred.customer_id, referred.ref_code bulk collect into customer_ids_to_delete, ref_codes_to_delete
        from customers_cz referred
        join customers_cz referee
            on referred.ref_code = referee.customer_id
        where referee.customer_id = mycustomer_id and ROWNUM <= 3;
        
    for i in customer_ids_to_delete.first .. customer_ids_to_delete.last loop
        update customers_cz
            set ref_code = null
            where mycustomer_id = ref_codes_to_delete(i);
    end loop;
exception
    when others then
        null;
end;


-- cancelOrder removes the cart from DB (empties cart)
create or replace procedure cancelOrder(cart_id_input orders_cz.cart_id%type)
as
begin
    delete from cart_products
        where cart_id = cart_id_input;
    delete from cart
        where cart_id = cart_id_input;
end;

create or replace procedure addToOrders(address_input varchar2, total_cost number, username_input login_cz.username%type)
as
    mycustomer_id orders_cz.customer_id%type;
    address varchar2(30) := address_input;
    newCart_id cart.cart_id%type := getHighestCartID();
begin
    --get customer id from username
    select customer_id into mycustomer_id from login_cz where username = username_input;

    if address is null then
        select address into address from customers_cz where customer_id = mycustomer_id;
    end if;

    insert into orders_cz
        values (newCart_id, address, total_cost, mycustomer_id, CURRENT_DATE, null);
end;


-- remove from stock
create or replace procedure removeFromStock(product_id_input products.product_id%type, quantity number)
as
    type myarray is varray(20)
        of number(4);
    ingredient_ids myarray;
begin
    select ingredient_id bulk collect into ingredient_ids
        from recipe_ingredients 
        join products
            using (product_id)
        where product_id = product_id_input;
    for j in ingredient_ids.first .. ingredient_ids.last loop
        update ingredients
            set stock = stock - quantity
            where ingredient_id = ingredient_ids(j);
    end loop;
    -- raise exception if no stock
end;

create or replace trigger noStock
before update on ingredients
for each row
declare 
    quantityReduced number(3);
begin
    if :NEW.stock < 0 then
        raise_application_error(-20001,'Error: Not enough stock for order');
    end if;
end;




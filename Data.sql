-- Data needs to be inserted in this order to make sure constraints (PK, FK) are not violated

-- Products
insert into Products (product_id, name, retail) values (1, 'Cappuccino', 5);
insert into Products (product_id, name, retail) values (2, 'Latte', 4);
insert into Products (product_id, name, retail) values (3, 'Espresso', 6);
insert into Products (product_id, name, retail) values (4, 'Mint Tea', 3);
insert into Products (product_id, name, retail) values (5, 'Blueberry Muffin', 3.50);
insert into Products (product_id, name, retail) values (6, 'Strawberry Croissant', 4.50);



--Ingredients
insert into Ingredients (ingredient_id, name, restock_price, stock) values (1, 'Coffee Beans', 87, 75);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (2, 'Water', 54, 999);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (3, 'Cream', 76, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (4, 'Milk', 76, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (5, 'Mint Tea Packet', 40, 80);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (6, 'Flour', 38, 20);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (7, 'Blueberries', 50, 30);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (8, 'Eggs', 80, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (9, 'Butter', 60, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (10, 'Sugar', 40, 70);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (11, 'Yeast', 35, 30);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (12, 'Strawberry Jam', 40, 20);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (13, 'Flavoured Syrup', 30, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (14, 'Baking Powder', 30, 70);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (15, 'Baking Soda', 30, 70);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (16, 'Salt', 40, 50);
insert into Ingredients (ingredient_id, name, restock_price, stock) values (17, 'Vanilla Extract', 30, 85);



-- Recipe_ingredients (bridging table)
-- Cappuccino
insert into recipe_ingredients (product_id, ingredient_id) values (1, 1);
insert into recipe_ingredients (product_id, ingredient_id) values (1, 2);
insert into recipe_ingredients (product_id, ingredient_id) values (1, 4);

-- Latte
insert into recipe_ingredients (product_id, ingredient_id) values (2, 1);
insert into recipe_ingredients (product_id, ingredient_id) values (2, 2);
insert into recipe_ingredients (product_id, ingredient_id) values (2, 3);

-- Espresso
insert into recipe_ingredients (product_id, ingredient_id) values (3, 1);
insert into recipe_ingredients (product_id, ingredient_id) values (3, 2);

-- Mint Tea
insert into recipe_ingredients (product_id, ingredient_id) values (4, 2);
insert into recipe_ingredients (product_id, ingredient_id) values (4, 5);

-- Blueberry Muffin
insert into recipe_ingredients (product_id, ingredient_id) values (5, 7);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 10);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 6);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 4);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 9);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 14);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 15);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 16);
insert into recipe_ingredients (product_id, ingredient_id) values (5, 17);

-- Strawberry Croissant
insert into recipe_ingredients (product_id, ingredient_id) values (6, 8);
insert into recipe_ingredients (product_id, ingredient_id) values (6, 9);
insert into recipe_ingredients (product_id, ingredient_id) values (6, 10);
insert into recipe_ingredients (product_id, ingredient_id) values (6, 11);
insert into recipe_ingredients (product_id, ingredient_id) values (6, 12);



-- Customers_CZ
insert into Customers_CZ (customer_id, address, phone, ref_code, Fname, Lname) values (1001, '9 Pine View Junction', '3748074753', null, 'Janel', 'Boriston');
insert into Customers_CZ (customer_id, address, phone, ref_code, Fname, Lname) values (1002, '62552 Kropf Terrace', '1307100006', null, 'Bard', 'Chawkley');
insert into Customers_CZ (customer_id, address, phone, ref_code, Fname, Lname) values (1003, '50 Bellgrove Court', '6185646295', 1001, 'Holly-anne', 'Godfrey');



-- Login_CZ test accounts
insert into Login_CZ (customer_id, username, password, salt) values (1001, 'steve1', '[B@6bf2d08e', '9nmsn0lt1232hhfd9r02dk8u0s'); -- real password: password1
insert into Login_CZ (customer_id, username, password, salt) values (1002, 'steve2', '[B@504bae78', '9nmsn0lt1232hhfd9r02dk8u0s'); -- real password: password2
insert into Login_CZ (customer_id, username, password, salt) values (1003, 'steve3', '[B@58d25a40', 'f7akv90q523ck4q174ihp5i9oh'); -- real password: password3



-- Cart
/*
insert into Orders_CZ (cart_id, item_count) values (1, 3);
insert into Orders_CZ (cart_id, item_count) values (2, 5);
insert into Orders_CZ (cart_id, item_count) values (3, 1);
insert into Orders_CZ (cart_id, item_count) values (4, 21);
insert into Orders_CZ (cart_id, item_count) values (5, 2);
insert into Orders_CZ (cart_id, item_count) values (6, 1);
insert into Orders_CZ (cart_id, item_count) values (7, 11);
insert into Orders_CZ (cart_id, item_count) values (8, 1);
insert into Orders_CZ (cart_id, item_count) values (9, 1);
insert into Orders_CZ (cart_id, item_count) values (10, 1);
*/
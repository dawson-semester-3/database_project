package application;

import java.net.URL;
import java.util.ResourceBundle;

import backend.Registration;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class RegistrationController extends CoffeeZoneApp implements Initializable {
	@FXML
	private Button goBack = new Button();

	@FXML
	private Text error = new Text();

	@FXML
	private TextField username = new TextField();
	@FXML
	private TextField password = new TextField();
	@FXML
	private TextField address = new TextField();
	@FXML
	private TextField phone = new TextField();
	@FXML
	private TextField Fname = new TextField();
	@FXML
	private TextField Lname = new TextField();

	@FXML
	private TextField referralCode = new TextField();

	@FXML
	private Text newReferralCode = new Text();

	@FXML
	private Button submit = new Button();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	@FXML
	public void submit() {
		try {
			boolean result = Registration.register(username.getText(), password.getText(), address.getText(), phone.getText(), referralCode.getText(),
					Fname.getText(), Lname.getText());
			newReferralCode.setText("YOUR REFERRAL CODE: " + Registration.newReferralCode); // assuming previous statement executed properly
			System.out.println("Registration: " + result);
		} catch (Exception e) {
			error.setFill(Color.BLUE);
			error.setText(e.getMessage());
		}
	}

	@FXML
	public void goBack() {
		stageView.setScene(login);
	}
}

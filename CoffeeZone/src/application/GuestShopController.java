package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class GuestShopController extends CoffeeZoneApp implements Initializable {
	@FXML
	private Button goBack = new Button();

	@FXML
	private ImageView img1 = new ImageView();
	@FXML
	private ImageView img2 = new ImageView();
	@FXML
	private ImageView img3 = new ImageView();
	@FXML
	private ImageView img4 = new ImageView();
	@FXML
	private ImageView img5 = new ImageView();
	@FXML
	private ImageView img6 = new ImageView();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// images array comes from CoffeeZoneApp class
		img1.setImage(images[0]);
		img2.setImage(images[1]);
		img3.setImage(images[2]);
		img4.setImage(images[3]);
		img5.setImage(images[4]);
		img6.setImage(images[5]);
	}

	@FXML
	public void goBack() {
		stageView.setScene(login);
	}
}
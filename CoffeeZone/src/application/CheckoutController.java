package application;

import java.net.URL;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ResourceBundle;

import backend.Checkout;
import backend.DBService;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CheckoutController extends CoffeeZoneApp implements Initializable {
	@FXML
	private Button goBack = new Button();
	@FXML
	private Button confirmOrderBtn = new Button();
	@FXML
	private Button cancelOrderBtn = new Button();

	@FXML
	private TextField cappuccinoQty = new TextField();
	@FXML
	private TextField latteQty = new TextField();
	@FXML
	private TextField espressoQty = new TextField();
	@FXML
	private TextField mintTeaQty = new TextField();
	@FXML
	private TextField blueberryMuffinQty = new TextField();
	@FXML
	private TextField strawberryCroissantQty = new TextField();

	@FXML
	private TextField cappuccinoCost = new TextField();
	@FXML
	private TextField latteCost = new TextField();
	@FXML
	private TextField espressoCost = new TextField();
	@FXML
	private TextField mintTeaCost = new TextField();
	@FXML
	private TextField blueBerryMuffinCost = new TextField();
	@FXML
	private TextField strawberryCroissantCost = new TextField();

	@FXML
	private Text referralCredit = new Text();

	@FXML
	private TextField totalCost = new TextField();

	@FXML
	private TextField shippingAddress = new TextField();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		int[] values = Checkout.getCartValues();
		// adding empty string to convert double to string
		// set quantity in checkout
		cappuccinoQty.setText(values[0] + "");
		latteQty.setText(values[1] + "");
		espressoQty.setText(values[2] + "");
		mintTeaQty.setText(values[3] + "");
		blueberryMuffinQty.setText(values[4] + "");
		strawberryCroissantQty.setText(values[5] + "");

		// set cost in checkout
		cappuccinoCost.setText("$" + 5.00 * values[0]);
		latteCost.setText("$" + 4.00 * values[1]);
		espressoCost.setText("$" + 6.00 * values[2]);
		mintTeaCost.setText("$" + 3.00 * values[3]);
		blueBerryMuffinCost.setText("$" + 3.50 * values[4]);
		strawberryCroissantCost.setText("$" + 4.50 * values[5]);

		// Referral Credit
		// check how many customers this customer has referred
		int hasReferredCount = 0;
		try {
			CallableStatement cs = DBService.conn.prepareCall("{? = call hasReferredCount(?)}");
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setString(2, usernameUsed);
			cs.execute();
			hasReferredCount = cs.getInt(1);
		} catch (SQLException e) {
		}

		double referralCreditDiscount = hasReferredCount * 5;
		referralCredit.setText("$" + referralCreditDiscount);

		double total = Double.parseDouble(cappuccinoCost.getText().substring(1)) + Double.parseDouble(latteCost.getText().substring(1))
				+ Double.parseDouble(espressoCost.getText().substring(1)) + Double.parseDouble(mintTeaCost.getText().substring(1))
				+ Double.parseDouble(blueBerryMuffinCost.getText().substring(1)) + Double.parseDouble(strawberryCroissantCost.getText().substring(1))
				- referralCreditDiscount;
		totalCost.setText("$" + total);
	}

	@FXML
	public void goBack() {
		stageView.setScene(shop);
	}

	@FXML
	public void cancelOrder() {
		// delete record from cart
		Checkout.cancelOrder();
		// back to shop
		stageView.setScene(login);
	}

	@FXML
	public void confirmOrder() {
		String address = shippingAddress.getText();
		if (shippingAddress.getText().isEmpty() || shippingAddress.getText().isBlank())
			address = null;
		Checkout.addToOrders(address, Double.parseDouble(totalCost.getText().substring(1)), usernameUsed);

		// remove from ingredients table
		try {
			Checkout.removeFromStock(1, Integer.parseInt(cappuccinoQty.getText()));
			Checkout.removeFromStock(2, Integer.parseInt(latteQty.getText()));
			Checkout.removeFromStock(3, Integer.parseInt(espressoQty.getText()));
			Checkout.removeFromStock(4, Integer.parseInt(mintTeaQty.getText()));
			Checkout.removeFromStock(5, Integer.parseInt(blueberryMuffinQty.getText()));
			Checkout.removeFromStock(6, Integer.parseInt(strawberryCroissantQty.getText()));
		} catch (SQLException e) {
			orderError = true;
		}

		// change scene
		try {
			Parent messageFXML = FXMLLoader.load(getClass().getResource("Message.fxml"));
			stageView.setScene(new Scene(messageFXML));
		} catch (Exception e) {
			e.printStackTrace();
		}

		// remove ref code from customers table
		try {
			CallableStatement cs = DBService.conn.prepareCall("{call removeRefCode(?)}");
			cs.setString(1, usernameUsed);
			cs.executeUpdate();
		} catch (SQLException e) {
		}
	}

}

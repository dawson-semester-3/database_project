package application;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class CoffeeShop extends Application {
	private Scene scene1, scene2, scene3, scene4;

	@Override
	public void start(Stage stage) {
		// Creating Scene1 for Login page
		GridPane gridpane1 = loginMethod();
		GridPane gridpane2 = shopMethod();
		GridPane gridpane3 = cartMethod();
		GridPane gridpane4 = registrationMethod();

		// Creating buttons to switch scenes
		Button switchScene1 = new Button("Click to go back to login");
		Button switchScene2 = new Button("Click to go to the shop");
		Button switchScene3 = new Button("Click to go to your cart");
		Button switchScene4 = new Button("Click to go to registration");

		switchScene1.setOnAction(e -> stage.setScene(scene1));
		switchScene2.setOnAction(e -> stage.setScene(scene2));
		switchScene3.setOnAction(e -> stage.setScene(scene3));
		switchScene4.setOnAction(e -> stage.setScene(scene4));

		// Switch scene 1 --> switch to scene 1 or 4
		gridpane1.add(switchScene2, 5, 5);

		// Switch scene 2 --> Switch to scene 1 or 3
		Button button1 = new Button("Click to go back to login");
		Button button2 = new Button("Click to go to the shop");
		button1.setOnAction(e -> stage.setScene(scene1));
		button2.setOnAction(e -> stage.setScene(scene2));
		gridpane2.add(button1, 0, 0);
		gridpane2.add(button2, 1, 1);

		// Scene 3 --> Switch to scene 2 or 4
		gridpane3.add(switchScene2, 5, 5);
		gridpane4.add(switchScene4, 5, 5);

		// Scene 4 --> Switch to scene 3
		gridpane4.add(switchScene3, 5, 5);

		Group root1 = new Group();
		root1.getChildren().add(gridpane1);

		Group root2 = new Group();
		root2.getChildren().add(new VBox(gridpane2));

		Group root3 = new Group();
		root3.getChildren().add(gridpane3);

		scene1 = new Scene(root1, 650, 450);
		scene2 = new Scene(root2, 650, 450);
		scene3 = new Scene(root3, 650, 450);

		Color c = Color.rgb(60, 15, 255);
		scene1.setFill(c);
		scene2.setFill(c);
		scene3.setFill(c);

		// associate scene to stage and show
		stage.setTitle("Coffee Shop");
		stage.setScene(scene2);
		stage.show();

	}

	public static GridPane loginMethod() {
		GridPane gridpane = new GridPane();
		Text t = new Text();
		t.setText("Coffee Shop User Login");
		t.setFont(Font.font("Verdana", 25));

		Color a = Color.rgb(187, 140, 228);
		t.setFill(a);

		gridpane.add(t, 0, 0, 2, 1);

		Text text1 = new Text("USERNAME: ");
		text1.setFill(a);
		gridpane.add(text1, 0, 1);

		TextField username = new TextField();
		username.setPromptText("Input your username");
		gridpane.add(username, 1, 1);

		Text text2 = new Text("PASSWORD: ");
		text2.setFill(a);
		gridpane.add(text2, 0, 2);

		TextField password = new TextField();
		password.setPromptText("Input your password");
		gridpane.add(password, 1, 2);

		Button submit = new Button("Log-in");
		gridpane.add(submit, 0, 3);

		gridpane.setAlignment(Pos.CENTER);
		gridpane.setPadding(new Insets(30, 30, 30, 30));
		gridpane.setHgap(6);
		gridpane.setVgap(6);

		return gridpane;
	}

	public static GridPane shopMethod() {
		GridPane gridpane = new GridPane();

		return gridpane;
	}

	public static GridPane cartMethod() {
		GridPane gridpane = new GridPane();
		return gridpane;
	}

	public static GridPane registrationMethod() {
		GridPane gridpane = new GridPane();
		return gridpane;
	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}

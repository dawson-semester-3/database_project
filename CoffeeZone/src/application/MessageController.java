package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class MessageController extends CoffeeZoneApp implements Initializable {
	@FXML
	private Button goToLoginBtn = new Button();

	@FXML
	private TextField message = new TextField();

	@FXML
	public void goToLogin() {
		stageView.setScene(login);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		if (orderError == true) {
			message.setFont(Font.font("Comic Sans MS", FontWeight.NORMAL, 32));
			message.setText("Error: Not enough stock for order :(");
		}
	}
}

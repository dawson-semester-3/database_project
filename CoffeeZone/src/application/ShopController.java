package application;

import java.net.URL;
import java.util.ResourceBundle;

import backend.Shop;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class ShopController extends CoffeeZoneApp implements Initializable {
	@FXML
	private Button goBack = new Button();

	@FXML
	private Button cappuccino = new Button();
	@FXML
	private Button latte = new Button();
	@FXML
	private Button espresso = new Button();
	@FXML
	private Button mintTea = new Button();
	@FXML
	private Button blueberryMuffin = new Button();
	@FXML
	private Button strawberryCroissant = new Button();

	@FXML
	private TextField cappuccinoQty = new TextField();
	@FXML
	private TextField latteQty = new TextField();
	@FXML
	private TextField mintTeaQty = new TextField();
	@FXML
	private TextField espressoQty = new TextField();
	@FXML
	private TextField blueberryMuffinQty = new TextField();
	@FXML
	private TextField strawberryCroissantQty = new TextField();

	@FXML
	private ImageView img1 = new ImageView();
	@FXML
	private ImageView img2 = new ImageView();
	@FXML
	private ImageView img3 = new ImageView();
	@FXML
	private ImageView img4 = new ImageView();
	@FXML
	private ImageView img5 = new ImageView();
	@FXML
	private ImageView img6 = new ImageView();

	@FXML
	private Button checkoutBtn = new Button();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// images array comes from CoffeeZoneApp class
		img1.setImage(images[0]);
		img2.setImage(images[1]);
		img3.setImage(images[2]);
		img4.setImage(images[3]);
		img5.setImage(images[4]);
		img6.setImage(images[5]);
	}

	@FXML
	public void goBack() {
		stageView.setScene(login);
	}

	@FXML
	public void addCappuccino() {
		int qty = 0;
		try {
			qty = Integer.parseInt(cappuccinoQty.getText());
		} catch (NumberFormatException e) {
		}
		cappuccinoQty.setText(qty + 1 + "");
	}
	@FXML
	public void addLatte() {
		int qty = 0;
		try {
			qty = Integer.parseInt(latteQty.getText());
		} catch (NumberFormatException e) {
		}
		latteQty.setText(qty + 1 + "");
	}
	@FXML
	public void addEspresso() {
		int qty = 0;
		try {
			qty = Integer.parseInt(espressoQty.getText());
		} catch (NumberFormatException e) {
		}
		espressoQty.setText(qty + 1 + "");
	}
	@FXML
	public void addMintTea() {
		int qty = 0;
		try {
			qty = Integer.parseInt(mintTeaQty.getText());
		} catch (NumberFormatException e) {
		}
		mintTeaQty.setText(qty + 1 + "");
	}
	@FXML
	public void addBlueberryMuffin() {
		int qty = 0;
		try {
			qty = Integer.parseInt(blueberryMuffinQty.getText());
		} catch (NumberFormatException e) {
		}
		blueberryMuffinQty.setText(qty + 1 + "");
	}
	@FXML
	public void addStrawberryCroissant() {
		int qty = 0;
		try {
			qty = Integer.parseInt(strawberryCroissantQty.getText());
		} catch (NumberFormatException e) {
		}
		strawberryCroissantQty.setText(qty + 1 + "");
	}

	@FXML
	public void goCheckout() {
		int cappuccinoCartQty;
		try {
			cappuccinoCartQty = Integer.parseInt(cappuccinoQty.getText());
		} catch (NumberFormatException e) {
			cappuccinoCartQty = 0;
		}

		int latteCartQty;
		try {
			latteCartQty = Integer.parseInt(latteQty.getText());
		} catch (NumberFormatException e) {
			latteCartQty = 0;
		}

		int espressoCartQty;
		try {
			espressoCartQty = Integer.parseInt(espressoQty.getText());
		} catch (NumberFormatException e) {
			espressoCartQty = 0;
		}

		int mintTeaCartQty;
		try {
			mintTeaCartQty = Integer.parseInt(mintTeaQty.getText());
		} catch (NumberFormatException e) {
			mintTeaCartQty = 0;
		}

		int blueberryMuffinCartQty;
		try {
			blueberryMuffinCartQty = Integer.parseInt(blueberryMuffinQty.getText());
		} catch (NumberFormatException e) {
			blueberryMuffinCartQty = 0;
		}

		int strawberryCroissantCartQty;
		try {
			strawberryCroissantCartQty = Integer.parseInt(strawberryCroissantQty.getText());
		} catch (NumberFormatException e) {
			strawberryCroissantCartQty = 0;
		}

		// add to cart
		Shop.addToCart(cappuccinoCartQty, latteCartQty, espressoCartQty, mintTeaCartQty, blueberryMuffinCartQty, strawberryCroissantCartQty);

		try {
			Parent checkoutFXML = FXMLLoader.load(getClass().getResource("Checkout.fxml"));
			stageView.setScene(new Scene(checkoutFXML));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// cart (validate to make sure all quantities != 0)

}
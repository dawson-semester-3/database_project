package application;

import backend.Shop;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class CoffeeZoneApp extends Application {
	public static String usernameUsed;
	public static Stage stageView;
	public static boolean orderError;

	public static Scene login;
	public static Scene registration;
	public static Scene shop;
	public static Scene guestShop;
	public static Scene checkout;
	public static Scene message;
	public static Image[] images;

	public static void main(String[] args) {
		launch(args);
	}

	private Scene createScene(String fxmlFile) {
		try {
			Parent loginFXML = FXMLLoader.load(getClass().getResource(fxmlFile));
			return new Scene(loginFXML);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void start(Stage stage) throws Exception {
		stageView = stage;
		stageView.setTitle("CoffeeZone");

		images = Shop.getImages();

		// Creating different scenes here, but not the ones that need user input's information
		login = createScene("Login.fxml");
		registration = createScene("Registration.fxml");
		shop = createScene("Shop.fxml");
		guestShop = createScene("GuestShop.fxml");
		// checkout = createScene("Checkout.fxml");
		// message = createScene("Message.fxml");

		// first scene = login
		stageView.setScene(login);
		stageView.setResizable(false);
		stageView.setOpacity(0.9);
		stageView.show();
	}
}

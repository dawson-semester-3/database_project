package application;

import java.net.URL;
import java.util.ResourceBundle;

import backend.Login;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class LoginController extends CoffeeZoneApp implements Initializable {
	@FXML
	private TextField username = new TextField();
	@FXML
	private PasswordField password = new PasswordField();

	@FXML
	private Button login = new Button();
	@FXML
	private Button registerPage = new Button();

	@FXML
	private Button seeProducts = new Button();

	@FXML
	private Text loggedIn = new Text();

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
	}

	@FXML
	public void registerUser() {
		try {
			stageView.setScene(registration);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// login button
	@FXML
	public void loginUser() {
		// add user to connection_ log table
		Login.logConnection(username.getText());

		usernameUsed = username.getText();
		if (Login.login(usernameUsed, password.getText())) {
			loggedIn.setText("Logged in!");
			stageView.setScene(shop);
		}
	}

	@FXML
	public void guestShop() {
		stageView.setScene(guestShop);

	}
}

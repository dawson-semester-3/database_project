package backend;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login extends User {
	public static boolean login(String username, String password) {
		if (!userExists(username))
			return false;
		return checkCorrectPassword(username, password);
	}

	private static boolean checkCorrectPassword(String username, String password) {
		if (username == null || password == null)
			return false;

		// get salt from Login table
		try {
			String getSalt = "SELECT SALT FROM LOGIN_CZ WHERE USERNAME = ?";
			PreparedStatement ps = DBService.conn.prepareStatement(getSalt);
			ps.setString(1, username.toLowerCase());
			ResultSet rs = ps.executeQuery();
			rs.next();
			byte[] salt = rs.getString("SALT").getBytes();

			String newHashedPassword = hashPassword(password, salt);

			// get hashedPassword from Login table
			String getPassword = "SELECT PASSWORD FROM LOGIN_CZ WHERE PASSWORD = ?";
			PreparedStatement ps2 = DBService.conn.prepareStatement(getPassword);
			ps2.setString(1, newHashedPassword);
			ResultSet rs2 = ps2.executeQuery();
			rs2.next();
			String hashedPassword = rs2.getString("PASSWORD");

			if (newHashedPassword.equals(hashedPassword)) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void logConnection(String username) {
		try {
			// into Customers Table
			CallableStatement cs = DBService.conn.prepareCall("{call logConnection(?)}");
			cs.setString(1, username);
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
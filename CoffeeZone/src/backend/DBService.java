package backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * @author Neil Fisher (1939668)
 */
public class DBService {
	public static Connection conn = getConnection();

	public static Connection getConnection() {
		String url = "jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca";
		try {
			System.out.println("====== Connect to database ======");
			Scanner scan = new Scanner(System.in);
			System.out.print("User: ");
			String user = scan.next();
			System.out.print("Password: ");
			String password = scan.next();
			scan.close();
			System.out.println();
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}

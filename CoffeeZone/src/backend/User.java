package backend;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class User {
	public static int registerCustomerInDB(String username, String hashedPassword, byte[] salt, String address, String phone, String referralCode,
			String Fname, String Lname) {
		try {
			// into Customers Table
			CallableStatement cs = DBService.conn.prepareCall("{CALL registerCustomer(?, ?, ?, ?, ?, ?, ?, ?)}");
			cs.setString(1, username);
			cs.setString(2, hashedPassword);
			cs.setString(3, new String(salt));
			cs.setString(4, address);
			cs.setString(5, phone);
			if (referralCode == null)
				cs.setNull(6, Types.NULL);
			else
				cs.setString(6, referralCode);
			cs.setString(7, Fname);
			cs.setString(8, Lname);
			cs.executeUpdate();

			CallableStatement cs2 = DBService.conn.prepareCall("{? = call getHighestCustomerID()}");
			cs2.registerOutParameter(1, Types.INTEGER);
			cs2.execute();
			int getHighestCustomerID = cs2.getInt(1);
			return getHighestCustomerID;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static boolean userExists(String username) {
		try {
			String getUser = "SELECT USERNAME FROM LOGIN_CZ WHERE USERNAME = ?";
			PreparedStatement ps = DBService.conn.prepareStatement(getUser);
			ps.setString(1, username.toLowerCase());
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				// if the user specified is in the DB, return true
				if (username.equals(rs.getString("USERNAME").toLowerCase()))
					return true;
				break;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * checks if they were referred
	 * 
	 * @param  referralCode referral code that was entered by the user on registration
	 * @return              boolean representing if they were referred by another registered user
	 */
	public static boolean isReferred(String referralCode) {
		if (referralCode == null) {
			return false;
		}
		try {
			String getReferral = "SELECT REF_CODE FROM CUSTOMERS_CZ WHERE REF_CODE = ?";
			PreparedStatement ps = DBService.conn.prepareStatement(getReferral);
			ps.setString(1, referralCode);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString("REF_CODE") != null || rs.getString("REF_CODE").isEmpty())
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static byte[] generateSalt() {
		return new BigInteger(130, new SecureRandom()).toString().getBytes();
	}

	public static String hashPassword(String password, byte[] salt) {
		if (password == null || password.isEmpty() || salt == null)
			return null;

		// this algorithm showed in class didn't work; it generated a different hash every time even without
		// a random salt
		/*
		String hash_algorithm = "PBEWithSHA1AndDESede";
		PBEKeySpec spec = new PBEKeySpec((password).toCharArray());
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hash_algorithm);
			byte[] hash = skf.generateSecret(spec).getEncoded();
			return hash.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		*/

		try {
			KeySpec spec = new PBEKeySpec("password".toCharArray(), salt, 512, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Base64.Encoder encoder = Base64.getEncoder();
			// System.out.println("salt: " + new String(salt));
			// System.out.println("hash: " + encoder.encodeToString(hash));
			return encoder.encodeToString(hash);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}

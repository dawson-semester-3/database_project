package backend;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;

public class Checkout extends User {
	public static void cancelOrder() {
		try {
			CallableStatement cs = DBService.conn.prepareCall("{call cancelOrder(?)}");
			cs.setInt(1, Shop.cart_id);
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static int[] getCartValues() {
		try {
			int[] values = {0, 0, 0, 0, 0, 0};
			for (int i = 0; i < values.length; i++) {
				CallableStatement cs = DBService.conn.prepareCall("{? = call getCartValues(?, ?)}");
				cs.registerOutParameter(1, Types.INTEGER);
				cs.setInt(2, Shop.cart_id);
				cs.setInt(3, i + 1);
				cs.execute();
				values[i] = cs.getInt(1);
			}
			return values;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void addToOrders(String address, double total_cost, String usernameUsed) {
		try {
			CallableStatement cs = DBService.conn.prepareCall("{call addToOrders(?, ?, ?)}");
			cs.setString(1, address);
			cs.setDouble(2, total_cost);
			cs.setString(3, usernameUsed);
			cs.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void removeFromStock(int product_id, int quantity) throws SQLException {
		CallableStatement cs = DBService.conn.prepareCall("{call removeFromStock(?, ?)}");
		cs.setInt(1, product_id);
		cs.setInt(2, quantity);
		cs.executeUpdate();
	}
}

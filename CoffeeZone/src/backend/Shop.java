package backend;

import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javafx.scene.image.Image;

public class Shop {
	public static int cart_id;

	public static Image[] getImages() {
		try {
			Image[] images = new Image[6];
			// get images from the products table
			String getImages = "SELECT IMAGE FROM PRODUCTS WHERE ROWNUM <= 6";
			PreparedStatement ps = DBService.conn.prepareStatement(getImages);
			ResultSet rs = ps.executeQuery();
			int i = 0;
			while (rs.next() && i < 6) {
				InputStream bs = rs.getBinaryStream("IMAGE");
				images[i] = new Image(bs);
				i++;
			}
			return images;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void addToCart(int cappuccinoQty, int latteQty, int espressoQty, int mintTeaQty, int blueberryMuffinQty,
			int strawberryCroissantQty) {
		try {
			CallableStatement cs = DBService.conn.prepareCall("{call addToCart(?, ?, ?, ?, ?, ?)}");
			cs.setInt(1, cappuccinoQty);
			cs.setInt(2, latteQty);
			cs.setInt(3, espressoQty);
			cs.setInt(4, mintTeaQty);
			cs.setInt(5, blueberryMuffinQty);
			cs.setInt(6, strawberryCroissantQty);
			cs.execute();

			CallableStatement cs2 = DBService.conn.prepareCall("{? = call getHighestCartID()}");
			cs2.registerOutParameter(1, Types.INTEGER);
			cs2.execute();
			cart_id = cs2.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

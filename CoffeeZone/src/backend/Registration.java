package backend;

public class Registration extends User {
	public static int newReferralCode = -1;

	public static boolean register(String username, String password, String address, String phone, String referralCode, String Fname, String Lname)
			throws Exception {
		if (userExists(username))
			return false;

		byte[] salt = generateSalt();
		String hashedPassword = hashPassword(password, salt);
		// both the salt and the hash need to be saved to the database
		if (!username.isEmpty() && !password.isEmpty() && !address.isEmpty() && phone.length() == 10 && !Fname.isEmpty() && !Lname.isEmpty()
				&& password.length() >= 6) {
			if (isReferred(referralCode)) {
				newReferralCode = registerCustomerInDB(username, hashedPassword, salt, address, phone, referralCode, Fname, Lname);
				return true;
			}
			else {
				newReferralCode = registerCustomerInDB(username, hashedPassword, salt, address, phone, null, Fname, Lname);
				return true;
			}
		}
		else {
			throw new Exception("Input error (make sure all required fields are filled correctly)");
		}
	}

	/*
	public static void main(String[] args) {
		try {
			register("steve4", "password4", "127 McVey", "1234567890", null, "Steve", "Jobs");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
}
